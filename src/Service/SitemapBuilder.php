<?php

namespace App\Service;

use App\Entity\Account\Account;
use App\Entity\Account\Category;
use App\Entity\Review\AccountReview;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;



class Sitemap_Builder
{
    var $locale;

    const PROFILE_PAGE_ROUTE_NAME = 'web_company_profile';

    const SUB_CATEGORIES_ROUTE_NAMES = [
        Account::SUBTYPE_SHOP => 'app_web_category_shopcategory',
        Account::SUBTYPE_SERVICES => 'app_web_category_servicescategory',
    ];

    const MAIN_PAGES_ROUTE_NAMES = [
        'web_subscriptions',
        'app_web_category_list',
        'app_web_category_listshopcategories',
        'app_web_category_listservicescategories',
    ];

    public function __construct(
        private EntityManagerInterface $entityManager,
        private RequestStack $requestStack,
        private UrlGeneratorInterface $router
    ) {
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
        $this->router = $router;
        $this->locale = $this->requestStack->getCurrentRequest()->getLocale();
    }

    private function buildSitemapForWww() {
        $urls = $this->getCommonUrls();

        $this->generateHomeUrl($urls);
        $this->generateMainUrls($urls);
        $this->generateCategoryUrls($urls);
        $this->generateAccountsUrls($urls);

            return $this->build($urls);
    }

    private function build(array $urls): string
    {
        $sitemap = $this->getSitemapHeader();

        foreach ($urls as $url) {
            $sitemap .= $this->getEntry($url);
        }

        $sitemap .= $this->getSitemapFooter();
        $sitemap = str_replace('><', ">\n<", $sitemap);
        return $sitemap;
    }

    private function generateHomeUrl(array &$urls): void
    {
        $urls[] = right_trim(
            $this->router->generate(
                'web_homepage',
                [],
                UrlGeneratorInterface::ABSOLUTE_URL
            ),
            '/'
        );
    }

    private function generateMainUrls(array &$urls): void
    {
        foreach (SitemapBuilder::MAIN_PAGES_ROUTE_NAMES as $routingName) {
            $urls[] = $this->router->generate(
                $routingName,
                [],
                UrlGeneratorInterface::ABSOLUTE_URL
            );
        }
    }

    private function generateCategoryUrls(array &$urls): void
    {
        $categories = $this->getCategories();

        foreach ($categories as $category) {
            if (key_exists($category['subtype'], SitemapBuilder::SUB_CATEGORIES_ROUTE_NAMES)) {
                $urls[] = $this->router->generate(
                    SitemapBuilder::SUB_CATEGORIES_ROUTE_NAMES[$category['subtype']],
                    ['slug' => $category['slug']],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );
            }
        }
    }

    private function generateAccountsUrls(array &$urls): void
    {
        $accounts
            = $this->getAccounts();

        foreach ($accounts as $account) {
            $urls[] = $this->router->generate(
                SitemapBuilder::PROFILE_PAGE_ROUTE_NAME,
                ['slug' => $account['slug']],
                UrlGeneratorInterface::ABSOLUTE_URL
            );
        }
    }

    private function getEntry(string $url): string
    {
        return "<url><loc>{$url}</loc></url>";
    }

    private function getSitemapFooter(): int
    {
        return '</urlset>';
    }

    private function getSitemapHeader(): string
    {
        return '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    }

    private function getCategories(): array
    {
        return $this->entityManager->createQueryBuilder()
            ->select('category.subtype, category.slug')
            ->from(Category::class, 'category')
            ->where('category.language = :locale')
            ->setParameter('locale', $this->locale)
            ->orderBy('category.subtype', 'DESC')
            ->addOrderBy('category.slug', 'ASC')
            ->getQuery()
            ->getResult();
    }

    private function getAccounts(): array
    {
        return $this->entityManager->createQueryBuilder()
            ->select('account.slug')
            ->from(Account::class, 'account')
            ->join('account.reviews', 'review')
            ->join('account.settings', 'settings')
            ->where('account.verified = true')
            ->andWhere('account.blocked = false')
            ->andWhere('settings.companyProfilePageHidden = false')
            ->andWhere('account.reviewsCount > 0')
            ->andWhere('review.language = :locale')
            ->setParameter('locale', $this->locale)
            ->andWhere('review.status = :reviewStatus')
            ->setParameter('reviewStatus', AccountReview::STATUS_ENABLED)
            ->groupBy('account.slug')
            ->addGroupBy('review.language')
            ->having('COUNT(review.language) > 0')
            ->orderBy('account.slug', 'ASC')
            ->getQuery()
        ;
    }

    private function getCommonUrls(): array
    {
        if ($this->locale != 'pl') {
            $locale == $this->locale.".";
        } else {
            $locale = "";
        }
        $urls = [
            "https://{$locale}trustmate.io/contact",
            "https://{$locale}trustmate.io/pricing",
            "https://{$locale}trustmate.io/partnership",
            "https://{$locale}trustmate.io/find-company",
            "https://{$locale}trustmate.io/integrations",
            "https://{$locale}trustmate.io/client-contact",
            "https://{$locale}trustmate.io/surveys",
            "https://{$locale}trustmate.io/increase-sales",
            "https://{$locale}trustmate.io/our-reviews",
            "https://{$locale}trustmate.io/google",
            "https://{$locale}trustmate.io/collecting-reviews",
        ];
        $localizedUrls = [
            'pl' => [
                "https://trustmate.io/references",
                "https://trustmate.io/porownanie/trustmate-vs-opineo",
                "https://trustmate.io/porownanie/trustmate-vs-zaufane",
                "https://trustmate.io/porownanie/trustmate-vs-trustedshops",
                "https://trustmate.io/jobs",
            ],
        ];

        if (array_key_exists($this->locale, $localizedUrls)) {
            $urls = array_merge($urls, $localizedUrls[$this->locale]);
        }

        return $urls;
    }
}
