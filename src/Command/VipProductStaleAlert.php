<?php

namespace App\Command\Cron\Internal;

use App\Repository\Product\ProductRepository;
use App\Repository\SubscriptionRepository;
use DateTime;
use Doctrine\ORM\AbstractQuery;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class VipProductStaleAlert extends Command
{
    public function __construct(
        private ProductRepository $productRepository,
        private SubscriptionRepository $subscriptionRepository,
        private HttpClientInterface $httpClient,
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:internal:vip-product-stale-alert')
            ->setDescription('Alarm about stale product base');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $accounts = $this->subscriptionRepository->getBaseAccountDataByPayingPrice();


        return self::SUCCESS;
    }

    private function sendAlert(string $message): void
    {
        // assume this sends valid message
    }
}
