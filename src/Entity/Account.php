<?php

namespace App\Entity\Account;

use App\Entity\Address;
use App\Entity\Invitation\Config;
use App\Entity\Invitation\Invitation;
use App\Entity\Review\AccountFeature;
use App\Entity\Review\AccountReview;
use App\Entity\Review\GoogleProductReviewFeed;
use App\Entity\Review\Reviewable;
use App\Entity\Review\ReviewableTrait;
use App\Entity\Subscription\Subscription;
use App\Entity\Traits\CreatedAt;
use App\Entity\Traits\Id;
use App\Entity\Traits\SanitizedUrl;
use App\Entity\Traits\UpdatedAt;
use App\Entity\User;
use App\Roles;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountRepository")
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class Account implements UserInterface, Reviewable
{
    use ReviewableTrait;
    use Id;
    use SanitizedUrl;
    use CreatedAt;
    use UpdatedAt;

    const DECATHLON = 'decathlon-pl';

    const SOURCE_ORGANIC = 'organic';
    const SOURCE_SHOPER = 'shoper';
    const SOURCE_HOME = 'home';
    const SOURCE_MAGENTO = 'magento';
    const SOURCE_IAI = 'iai';
    const SOURCE_SKYSHOP = 'skyshop';
    const SOURCE_WOOCOMMERCE = 'woocommerce';
    const SOURCE_VENDERO = 'vendero';
    const SOURCE_PRESTA = 'presta';
    const SOURCE_REDCART = 'redcart';
    const SOURCE_OPENCART = 'opencart';
    const SOURCE_SHOPIFY = 'shopify';
    const SOURCE_APPSUMO = 'appsumo';
    const SOURCE_PANEL = 'panel';
    const SOURCE_SOTE = 'sote';
    const SUBTYPE_SHOP = 'shop';
    const SUBTYPE_SERVICES = 'services';
    const SUBTYPES = [
        self::SUBTYPE_SHOP,
        self::SUBTYPE_SERVICES,
    ];

    private const FRESH_ACCOUNT_CREATED_AT = '-30 days';
    private const FRESH_ACCOUNT_MAX_REVIEW_COUNT = 500;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"default", "publicProfile", "panel", "search", "affiliate"})
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string
     */
    protected $displayName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Subscription\Subscription", mappedBy="account", cascade={"persist", "remove"})
     * @ORM\OrderBy({"createdAt": "DESC"})
     */
    protected Collection $subscriptions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Account\AccountContact", mappedBy="account", cascade={"remove", "persist"})
     *
     * @var Collection
     */
    private $contacts;

    /**
     * @ORM\Column(type="string", length=1500, nullable=true)
     * @Groups({"panel", "publicProfile", "search"})
     *
     * @var string
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"panel", "publicProfile"})
     *
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Groups({"default", "owner", "panelOwnerProfile", "publicProfile", "panel", "search"})
     */
    protected $url;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Groups({"panel", "publicProfile"})
     *
     * @var string
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", nullable=true, options={"default": NULL})
     * @Groups({"panel", "publicProfile"})
     *
     * @var string
     */
    protected $facebookUrl;

    /**
     * @ORM\Column(type="string", nullable=true, options={"default": NULL})
     * @Groups({"panel", "publicProfile"})
     *
     * @var string
     */
    protected $instagramUrl;

    /**
     * @ORM\Embedded(class = "App\Entity\Address", columnPrefix = false)
     * @Groups({"panel", "publicProfile"})
     */
    protected $address;

    /**
     * @ORM\Embedded(class = "App\Entity\Address", columnPrefix = "registration_")
     * @Groups({"panel"})
     */
    protected $registrationAddress;

    /**
     * @ORM\Column(type="string", nullable=false, unique=true)
     * @Groups({"panel"})
     */
    protected $uuid;

    /**
     * @Vich\UploadableField(mapping="account_logo", fileNameProperty="logoName")
     *
     * @var File
     */
    protected $logoFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"publicProfile", "search"})
     *
     * @var string
     */
    protected $logoName;

    /**
     * @Groups({"publicProfile", "panel", "search", "default"})
     */
    protected $logoUrl;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"panel", "publicProfile", "search"})
     *
     * @var bool
     */
    private $verified = null;

    /**
     * @ORM\Column(type="string", nullable=true, unique=true)
     *
     * @var string
     */
    private $verificationToken = null;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": false})
     * @Groups({"panel", "publicProfile"})
     *
     * @var bool
     */
    private $blocked = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @Groups({"panel"})
     *
     * @var bool
     */
    private $createdByUser;

    /**
     * @ORM\Column(type="string", length=120, unique=true)
     * @Groups({"default", "panel", "publicProfile", "search"})
     * @Gedmo\Slug(fields={"sanitizedUrl"})
     */
    private $slug;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     * @Groups({"panel"})
     */
    private $clientHash;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Account\Notification", mappedBy="account")
     * @Groups({"panel"})
     */
    private $notifications;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     * @Groups({"panel"})
     */
    private $apiKey;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Account\Settings", mappedBy="account", cascade={"remove"})
     * @Groups({"settings", "publicProfile"})
     *
     * @var Settings
     */
    private $settings;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    public ?string $source = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    public ?string $platform = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OptOut", mappedBy="account")
     */
    public $optOuts;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="accounts", cascade={"PERSIST"})
     */
    private $users;

    /**
     * @ORM\Column(type="string", options={"length": 5})
     * @Assert\Language()
     * @Groups({"default"})
     *
     * @var string
     */
    private $language;

    /**
     * @ORM\Column(type="string", options={"length": 255}, nullable=true)
     */
    private ?string $platformId = null;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\AppSumo\Group", mappedBy="accounts")
     */
    private $appSumoGroups;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Account\AccountPartnership", mappedBy="account", cascade={"persist"})
     */
    private ?AccountPartnership $accountPartnership = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Review\AccountReview", mappedBy="itemReviewed", cascade={"persist"})
     *
     * @var Collection
     */
    private $reviews;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Invitation\Invitation", mappedBy="account", cascade={"persist"})
     */
    private Collection $invitations;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @Groups({"panel", "publicProfile"})
     */
    protected $primaryCategory;

    /**
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="accounts")
     * @ORM\JoinTable(
     *     name="account_account_category",
     * )
     *
     * @Groups({"panel", "publicProfile"})
     */
    protected $categories;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"publicProfile", "panel"})
     */
    protected $subtype;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     * @Groups({"publicProfile"})
     *
     * @var string
     */
    protected $nip;

    /**
     * @ORM\OneToMany(
     *     targetEntity="App\Entity\Review\AccountFeature",
     *     mappedBy="account",
     *     cascade={"persist", "remove"}
     * )
     * @Groups({"publicProfile", "panel", "panelAccountStatsProfile"})
     *
     * @var Collection
     */
    private $features;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Invitation\Config", mappedBy="account", cascade={"remove"})
     *
     * @var Collection
     */
    private $configs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Review\NetPromoterReview", mappedBy="account")
     *
     * @var Collection
     */
    protected $netPromoterReviews;

    /**
     * @var int
     */
    protected $invitationsCount = 0;

    /**
     * @ORM\Column(type="string", nullable=true, unique=true)
     */
    private ?string $stripeCustomerId = null;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private ?string $stripeCurrency = null;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default": 0})
     */
    private int $stripeBalance = 0;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Account\AccountAffiliate", mappedBy="account")
     */
    protected Collection $affiliates;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Account\AccountAffiliate", mappedBy="affiliate")
     */
    protected ?AccountAffiliate $affiliatedBy = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Statistics\WidgetDailyStats", mappedBy="account")
     */
    private Collection $widgetsDailyStats;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Review\GoogleProductReviewFeed", mappedBy="account", cascade={"remove"})
     */
    private ?GoogleProductReviewFeed $googleProductReviewFeed = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Account\Stats", mappedBy="account", cascade={"remove"})
     */
    private ?Stats $stats = null;

    /**
     * @ORM\Column(type="json", nullable=true, options={"jsonb": true})
     * @Groups({"publicProfile"})
     * @var string[]|null
     */
    private ?array $supportedLanguages = null;

    /**
     * @ORM\ManyToMany(targetEntity="CategoryTag", inversedBy="accounts")
     * @ORM\JoinTable(
     *     name="account_account_category_tag",
     * )
     * @Groups({"publicProfile"})
     */
    private Collection $categoryTags;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    protected ?User $owner = null;

    public function __construct()
    {
        $this->address = new Address();
        $this->registrationAddress = new Address();
        $this->subscriptions = new ArrayCollection();
        $this->createdAt = new DateTime();
        $this->updatedAt = new DateTime();
        $this->contacts = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->appSumoGroups = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->reviews = new ArrayCollection();
        $this->invitations = new ArrayCollection();
        $this->features = new ArrayCollection();
        $this->netPromoterReviews = new ArrayCollection();
        $this->affiliates = new ArrayCollection();
        $this->widgetsDailyStats = new ArrayCollection();
        $this->categoryTags = new ArrayCollection();
        $this->generateUuid();
        $this->generateApiKey();
    }

    public function __toString()
    {
        $name = $this->sanitizedUrl;

        if ($this->isBlocked()) {
            $name .= ' (b)';
        }

        return $name;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function serialize()
    {
        return serialize($this->id);
    }

    public function unserialize($serialized)
    {
        $this->id = unserialize($serialized);
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = preg_replace('/\s\s+/', ' ', trim($name));

        return $this;
    }

    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    public function setDisplayName(string $displayName): self
    {
        $this->displayName = preg_replace('/\s\s+/', ' ', trim($displayName));

        return $this;
    }

    /**
     * @Groups({"default", "publicProfile", "panel", "search"})
     */
    public function getPreferredName(): ?string
    {
        return $this->displayName ?: $this->getSanitizedUrl();
    }

    public function isContactDataComplete(): bool
    {
        return $this->phone
            && $this->email
            && $this->address->getStreet()
            && $this->address->getZipCode()
            && $this->address->getCity();
    }

    public function isRegistrationDataComplete(): bool
    {
        return $this->registrationAddress->getCountry()
            && $this->registrationAddress->getStreet()
            && $this->registrationAddress->getZipCode()
            && $this->registrationAddress->getCity();
    }

    public function getLastLogin(): ?DateTime
    {
        if ($user = $this->getBusinessOwner()) {
            return $user->getLastLogin();
        }

        return null;
    }

    public function buildFileUrls(UploaderHelper $uploaderHelper): void
    {
        $this->setLogoUrl($uploaderHelper->asset($this, 'logoFile'));
    }

    public function addSubscription(Subscription $subscription): self
    {
        $this->getSubscriptions()->add($subscription);
        $subscription->setAccount($this);

        return $this;
    }

    public function removeSubscription(Subscription $subscription): self
    {
        $this->getSubscriptions()->removeElement($subscription);

        return $this;
    }

    public function removeAllSubscriptions(): self
    {
        $this->subscriptions->clear();

        return $this;
    }

    public function getSubscriptions(): ?Collection
    {
        return $this->subscriptions;
    }

    public function setSubscriptions(Collection $subscriptions): self
    {
        $this->subscriptions = $subscriptions;

        return $this;
    }

    /**
     * @Groups({"panel"})
     */
    public function getActiveSubscription(): ?Subscription
    {
        /** @var Subscription $subscription */
        foreach ($this->subscriptions as $subscription) {
            if ($subscription->isActive()) {
                return $subscription;
            }
        }

        return null;
    }

    public function getLastSubscription(): ?Subscription
    {
        if ($this->subscriptions->isEmpty()) {
            return null;
        }

        $criteria = new Criteria();
        $expr = $criteria::expr();
        $criteria->orderBy(['paidUntil' => 'DESC']);
        $criteria->andWhere($expr->neq('paidUntil', null));

        return $this->subscriptions->matching($criteria)->first() ?: null;
    }

    /**
     * @Groups({"panel"})
     */
    public function isPaymentActive(): bool
    {
        $activeSubscription = $this->getActiveSubscription();
        if (null === $activeSubscription) {
            return true;
        }

        if ($activeSubscription->isRecurring()) {
            return false;
        }

        $lastSubscription = $this->getLastSubscription();

        return $lastSubscription === $activeSubscription || $lastSubscription->isOutdated();
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getContacts(): ?Collection
    {
        return $this->contacts;
    }

    public function addContact(AccountContact $contact): self
    {
        $contact->setAccount($this);
        $this->contacts->add($contact);

        return $this;
    }

    public function removeContact(AccountContact $contact): self
    {
        $this->contacts->remove($contact);

        return $this;
    }

    public function getContactByType(string $code): ?AccountContact
    {
        /** @var AccountContact $contact */
        foreach ($this->getContacts() as $contact) {
            if ($contact->getContactType() && $contact->getContactType()->getCode() === $code) {
                return $contact;
            }
        }

        return null;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getFacebookUrl(): ?string
    {
        return $this->facebookUrl;
    }

    public function setFacebookUrl(?string $facebookUrl): self
    {
        $this->facebookUrl = $facebookUrl;

        return $this;
    }

    public function getInstagramUrl(): ?string
    {
        return $this->instagramUrl;
    }

    public function setInstagramUrl(?string $instagramUrl): self
    {
        $this->instagramUrl = $instagramUrl;

        return $this;
    }

    public function getRegistrationAddress(): Address
    {
        return $this->registrationAddress;
    }

    public function setRegistrationAddress(Address $registrationAddress): self
    {
        $this->registrationAddress = $registrationAddress;

        return $this;
    }

    public function getAddress(): Address
    {
        return $this->address;
    }

    public function setAddress(Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    protected function generateUuid(): self
    {
        $this->uuid = Uuid::uuid4()->toString();

        return $this;
    }

    protected function generateApiKey(): self
    {
        $this->apiKey = Uuid::uuid4()->toString();

        return $this;
    }

    public function getLogoName(): ?string
    {
        return $this->logoName;
    }

    public function setLogoName(?string $logoName): self
    {
        $this->logoName = $logoName;

        return $this;
    }

    public function getLogoFile(): ?File
    {
        return $this->logoFile;
    }

    public function setLogoFile(?File $logoFile): self
    {
        $this->logoFile = $logoFile;
        $this->updateTimestamps();

        return $this;
    }

    public function getLogoUrl(): ?string
    {
        return $this->logoUrl;
    }

    public function setLogoUrl(?string $logoUrl): self
    {
        $this->logoUrl = $logoUrl;

        return $this;
    }

    public function isVerified(): ?bool
    {
        return $this->verified;
    }

    public function setVerified(bool $verified): Account
    {
        $this->verified = $verified;

        return $this;
    }

    public function isBlocked(): bool
    {
        return $this->blocked;
    }

    public function setBlocked(bool $blocked): self
    {
        $this->blocked = $blocked;

        if ($blocked) {
            foreach ($this->users as $user) {
                if (count($user->getAccounts()) === 1) {
                    $user->setEnabled(false);
                }
            }
        } else {
            if (count($this->users) === 1) {
                $this->users[0]->setEnabled(true);
            }
        }

        return $this;
    }

    public function isCreatedByUser(): bool
    {
        return $this->createdByUser;
    }

    public function setCreatedByUser(bool $createdByUser): self
    {
        $this->createdByUser = $createdByUser;

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getClientHash(): ?string
    {
        return $this->clientHash;
    }

    public function setClientHash(?string $clientHash): self
    {
        $this->clientHash = $clientHash;

        return $this;
    }

    public function getAdministratorContact(): ?AccountContact
    {
        return $this->getContactByType(ContactType::TYPE_ADMINISTRATOR);
    }

    public function getNotifications(): Collection
    {
        return $this->notifications;
    }

    public function setNotifications($notifications): self
    {
        $this->notifications = $notifications;

        return $this;
    }

    public function getNotificationByCode(string $code): ?Notification
    {
        foreach ($this->notifications as $notification) {
            if ($notification->getCode() === $code) {
                return $notification;
            }
        }

        return null;
    }

    public function getNotificationById(int $id): ?Notification
    {
        foreach ($this->notifications as $notification) {
            if ($notification->getId() === $id) {
                return $notification;
            }
        }

        return null;
    }

    public function getVerificationToken(): ?string
    {
        return $this->verificationToken;
    }

    public function setVerificationToken(?string $verificationToken): Account
    {
        $this->verificationToken = $verificationToken;

        return $this;
    }

    public function getApiKey(): ?string
    {
        return $this->apiKey;
    }

    public function setApiKey(string $apiKey): self
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    public function getSettings(): ?Settings
    {
        return $this->settings;
    }

    public function setSettings(Settings $settings): self
    {
        $this->settings = $settings;
        if (null === $settings->getAccount()) {
            $settings->setAccount($this);
        }

        return $this;
    }

    public function setSource(?string $source): self
    {
        if ($source == null) {
            $source = 'other'; // dirty hack to krobok errors TODO fix accounts with null sources
        }
        $this->source = $source;

        return $this;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user)
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->addAccount($this);
        }
    }

    public function removeUser(User $user)
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeAccount($this);
        }
    }

    public function getBusinessOwner(): ?User
    {
        // TODO: roles - change to getOwner();
        foreach ($this->getUsers() as $user) {
            if ($user->hasRole(Roles::BUSINESS_OWNER)) {
                return $user;
            }
        }

        return null;
    }

    /**
     * @Groups({"panel"})
     */
    public function isDecathlon(): bool
    {
        return self::DECATHLON === $this->slug;
    }

    public function getNegativeReviewAlertRecipients(): array
    {
        return $this->settings
            ? $this->settings->negativeReviewAlertRecipients
            : [];
    }

    public function getOptOutAlertRecipients(): array
    {
        return $this->settings
            ? $this->settings->optOutAlertRecipients
            : [];
    }

    public function getWeeklyReportRecipients(): array
    {
        return $this->settings
            ? $this->settings->getReportRecipients()
            : [];
    }

    /**
     * @Groups({"publicProfile"})
     */
    public function isCompanyProfilePageHidden(): bool
    {
        return $this->settings
            ? $this->settings->isCompanyProfilePageHidden()
            : false;
    }

    /**
     * @Groups({"publicProfile"})
     */
    public function isReviewsDisabledFromOrganic(): bool
    {
        return $this->settings
            ? $this->settings->isReviewsDisabledFromOrganic()
            : false;
    }


    public function getLanguage(): string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function fromAppSumo(): bool
    {
        return $this->appSumoGroups->count() > 0;
    }

    public function getAppSumoGroups(): Collection
    {
        return $this->appSumoGroups;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function getPlatform(): ?string
    {
        return $this->platform;
    }

    public function setPlatform(?string $platform): self
    {
        $this->platform = $platform;

        return $this;
    }

    public function getPlatformId(): ?string
    {
        return $this->platformId;
    }

    public function setPlatformId(?string $platformId): self
    {
        $this->platformId = $platformId;

        return $this;
    }

    public function getAccountPartnership(): ?AccountPartnership
    {
        return $this->accountPartnership;
    }

    public function setAccountPartnership(?AccountPartnership $accountPartnership): self
    {
        $this->accountPartnership = $accountPartnership;

        return $this;
    }

    public function getRoles(): array
    {
        return ['ROLE_API'];
    }

    public function getPassword(): ?string
    {
        return '';
    }

    public function getSalt(): ?string
    {
        return '';
    }

    public function getUsername(): string
    {
        return $this->slug;
    }

    public function getUserIdentifier(): string
    {
        return $this->getSlug();
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @Groups({"publicProfile", "search"})
     */
    public function isCooperating()
    {
        return $this->isVerified() && !$this->isCreatedByUser();
    }

    public function getReviews(): ?Collection
    {
        return $this->reviews;
    }

    public function setReviews(Collection $reviews): Account
    {
        $this->reviews = $reviews;

        return $this;
    }

    public function addReview(AccountReview $review): void
    {
        $this->reviews->add($review);
        $review->setItemReviewed($this);
    }

    public function getInvitations(): ?Collection
    {
        return $this->invitations;
    }

    public function setInvitations(Collection $invitations): self
    {
        $this->invitations = $invitations;

        return $this;
    }

    public function addInvitation(Invitation $invitation): self
    {
        if (!$this->invitations->contains($invitation)) {
            $this->invitations->add($invitation);
            $invitation->setAccount($this);
        }

        return $this;
    }

    public function getSuspendedInvitationsCount(): int
    {
        $criteria = new Criteria();
        $criteria->where(Criteria::expr()->eq('suspended', true));

        return $this->invitations->matching($criteria)->count();
    }

    public function getCategories(): ?Collection
    {
        return $this->categories;
    }

    public function setCategories(Collection $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    public function addCategory(Category $category): self
    {
        $this->categories->add($category);

        return $this;
    }

    public function getPrimaryCategory(): ?Category
    {
        return $this->primaryCategory;
    }

    public function setPrimaryCategory(?Category $primaryCategory): self
    {
        $this->features = new ArrayCollection();
        $this->primaryCategory = $primaryCategory;

        if (!$this->getFeatures()) {
            /*
             * @var Feature $feature
             */
            foreach ($this->primaryCategory->getFeatures() as $feature) {
                $businessAccountFeature = new AccountFeature();
                $businessAccountFeature->setFeature($feature);
                $this->addFeature($businessAccountFeature);
            }
        }

        return $this;
    }

    public function getSubtype(): ?string
    {
        return $this->subtype;
    }

    public function setSubtype(?string $subtype): self
    {
        $this->subtype = $subtype;

        return $this;
    }

    /**
     * @Groups({"panel"})
     */
    public function getNip(): ?string
    {
        return $this->nip;
    }

    public function setNip(?string $nip): self
    {
        $this->nip = $nip;

        return $this;
    }

    public function getFeatures(): Collection
    {
        return $this->features;
    }

    public function addFeature(AccountFeature $feature): self
    {
        $feature->setAccount($this);
        $this->features->add($feature);

        return $this;
    }

    public function removeFeature(AccountFeature $feature): self
    {
        $this->features->remove($feature);

        return $this;
    }

    public function getConfigs(): ?Collection
    {
        return $this->configs;
    }

    public function setConfigs(Collection $configs): Account
    {
        $this->configs = $configs;

        return $this;
    }

    public function getAutomaticConfigs(): ?Collection
    {
        return $this->configs->filter(function (Config $config) {
            return $config->isAutomatic();
        });
    }

    public function getNetPromoterReviews(): Collection
    {
        return $this->netPromoterReviews;
    }

    public function getInvitationsCount(): int
    {
        return $this->invitationsCount;
    }

    public function setInvitationsCount(int $invitationsCount): self
    {
        $this->invitationsCount = $invitationsCount;

        return $this;
    }

    public function getStripeCustomerId(): ?string
    {
        return $this->stripeCustomerId;
    }

    public function setStripeCustomerId(?string $stripeCustomerId): self
    {
        $this->stripeCustomerId = $stripeCustomerId;

        return $this;
    }

    public function getStripeCurrency(): ?string
    {
        return $this->stripeCurrency;
    }

    public function setStripeCurrency(?string $stripeCurrency): self
    {
        $this->stripeCurrency = $stripeCurrency;

        return $this;
    }

    public function getStripeBalance(): int
    {
        return $this->stripeBalance;
    }

    public function setStripeBalance(int $stripeBalance): self
    {
        $this->stripeBalance = $stripeBalance;

        return $this;
    }

    public function getAffiliates(): Collection
    {
        return $this->affiliates;
    }

    public function setAffiliates(ArrayCollection $affiliates): self
    {
        $this->affiliates = $affiliates;

        return $this;
    }

    public function addAffiliate(AccountAffiliate $affiliate): self
    {
        if (!$this->affiliates->contains($affiliate)) {
            $this->affiliates->add($affiliate);
            $affiliate->setAccount($this);
        }

        return $this;
    }

    public function getUnusedAffiliates(): Collection
    {
        $criteria = new Criteria();
        $expr = $criteria::expr();
        $criteria
            ->where($expr->eq('affiliateValid', true))
            ->andWhere($expr->eq('accountCreditSent', false));

        return $this->affiliates->matching($criteria);
    }

    public function getValidAffiliates(bool $valid = true): Collection
    {
        $criteria = new Criteria();
        $expr = $criteria::expr();
        $criteria
            ->where($expr->eq('affiliateValid', $valid));

        return $this->affiliates->matching($criteria);
    }

    public function getAffiliatedBy(): ?AccountAffiliate
    {
        return $this->affiliatedBy;
    }

    public function setAffiliatedBy(?AccountAffiliate $affiliatedBy): self
    {
        $this->affiliatedBy = $affiliatedBy;
        $affiliatedBy->setAffiliate($this);

        return $this;
    }

    public function getWidgetsDailyStats(): Collection
    {
        return $this->widgetsDailyStats;
    }

    public function getGoogleProductReviewFeed(): ?GoogleProductReviewFeed
    {
        return $this->googleProductReviewFeed;
    }

    public function setGoogleProductReviewFeed(?GoogleProductReviewFeed $googleProductReviewFeed): self
    {
        $this->googleProductReviewFeed = $googleProductReviewFeed;

        return $this;
    }

    public function detachStripe(): self
    {
        $this->setStripeCurrency(null);
        $this->setStripeBalance(0);
        $this->setStripeCustomerId(null);

        return $this;
    }

    public function isFresh(): bool
    {
        return !$this->isBlocked() &&
            !$this->isCreatedByUser() &&
            $this->isVerified() &&
            $this->getReviewsCount() < self::FRESH_ACCOUNT_MAX_REVIEW_COUNT &&
            $this->getCreatedAt() > new DateTime(self::FRESH_ACCOUNT_CREATED_AT);
    }

    public function getStats(): ?Stats
    {
        return $this->stats;
    }

    public function setStats(?Stats $stats): self
    {
        $this->stats = $stats;

        return $this;
    }

    public function getSupportedLanguages(): ?array
    {
        return $this->supportedLanguages;
    }

    public function setSupportedLanguages(?array $supportedLanguages): self
    {
        $this->supportedLanguages = $supportedLanguages;

        return $this;
    }

    public function getCategoryTags(): Collection
    {
        return $this->categoryTags;
    }

    public function addCategoryTag(CategoryTag $categoryTag): self
    {
        if (!$this->categoryTags->contains($categoryTag)) {
            $this->categoryTags->add($categoryTag);
        }

        return $this;
    }

    public function removeCategoryTag(CategoryTag $categoryTag): self
    {
        if ($this->categoryTags->contains($categoryTag)) {
            $this->categoryTags->removeElement($categoryTag);
        }

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function isOwnedBy(?User $user): bool
    {
        return $this->getOwner() === $user;
    }

    public function isServiceAccount(): bool
    {
        return $this->getSubtype() === self::SUBTYPE_SERVICES;
    }
}
