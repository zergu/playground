<?php

namespace App\Entity\Product;

use App\Entity\Account\Account;
use App\Entity\Review\ProductTraitAverageGrade;
use App\Entity\Review\Reviewable;
use App\Entity\Review\ReviewableTrait;
use App\Entity\Traits\CreatedAt;
use App\Entity\Traits\Name;
use App\Entity\Traits\UpdatedAt;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Product\ProductRepository")
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"account_id", "local_id"})
 * },indexes={
 *     @ORM\Index(columns={"account_id", "group_id"}, name="product_account_group_idx", options={"where": "(group_id IS NOT NULL)"}),
 * })
 */
class Product implements Reviewable, Traitable
{
    use Name;
    use CreatedAt;
    use UpdatedAt;
    use ReviewableTrait;

    public const GTIN_MIN_LENGTH = 8;
    public const GTIN_MAX_LENGTH = 14;
    public const GTIN_MAX_COUNT = 32;

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue
     * @Groups({"default", "publicProfile", "panel", "search", "invitationConfig"})
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"default", "form", "api"})
     * @Assert\Length(min=2)
     */
    private ?string $brand = null;

    /**
     * @ORM\Column(type="string", length=360, nullable=true)
     * @Groups({"default", "publicProfile", "form", "api"})
     * @Assert\Length(min=11)
     * @Assert\Url
     */
    private ?string $imageUrl = null;

    /**
     * @ORM\Column(type="string", length=360, nullable=true)
     * @Groups({"default", "publicProfile", "form", "api"})
     * @Assert\Length(min=11)
     * @Assert\Url
     */
    private ?string $imageThumbUrl = null;

    /**
     * @ORM\Column(type="string", length=360, nullable=true)
     * @Groups({"default", "api"})
     * @Assert\Url
     * @Assert\Length(min=11)
     */
    private ?string $productUrl = null;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     * @Groups({"form", "default", "publicProfile", "api"})
     * @Assert\Length(min=8, max=512)
     *
     * max 32 gtins + separation sign
     *
     * @see https://www.gs1.org/docs/idkeys/GS1_GTIN_Executive_Summary.pdf
     */
    private ?string $gtin = null;

    /**
     * @ORM\Column(type="string", length=70, nullable=true)
     * @Groups({"form", "default", "publicProfile", "api"})
     * @Assert\Length(min=1, max=70)
     */
    private ?string $mpn = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"form", "default", "publicProfile", "api"})
     * @Assert\Length(max=255)
     */
    private ?string $sku = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Groups({"default", "publicProfile", "api"})
     * @Assert\Length(min=1)
     */
    private string $localId;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(min=1)
     * @Groups({"default", "api"})
     */
    private ?string $groupId = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Account\Account")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"form"})
     */
    private Account $account;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product\Category", inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"default", "form", "publicProfile", "api"})
     */
    private Category $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Review\ProductTraitAverageGrade", mappedBy="product", cascade={"persist", "remove"})
     * @Groups({"api"})
     * @ORM\OrderBy({"updatedAt": "DESC"})
     */
    private Collection $traitAverages;

    /**
     * @ORM\Column(type="string", nullable=true, length=30)
     */
    private ?string $sourceType = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?DateTimeImmutable $lastReviewAt = null;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"api"})
     */
    private ?bool $deactivated = null;

    public ?int $expertReviewsApproved = null;

    public ?int $expertReviewsInProgress = null;

    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->traitAverages = new ArrayCollection();
    }

    /**
     * @Groups({"traits"})
     */
    public function hasTraits(): bool
    {
        return !$this->getTraitAverages()->isEmpty();
    }

    public function hasImage(): bool
    {
        return (bool) $this->imageUrl;
    }

    public function hasProductUrl(): bool
    {
        return (bool) $this->productUrl;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(?string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function setImageUrl(?string $imageUrl): self
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    public function getImageThumbUrl(): ?string
    {
        return $this->imageThumbUrl;
    }

    public function setImageThumbUrl(?string $imageThumbUrl): self
    {
        $this->imageThumbUrl = $imageThumbUrl;

        return $this;
    }

    public function getProductUrl(): ?string
    {
        return $this->productUrl;
    }

    public function setProductUrl(?string $productUrl): self
    {
        $this->productUrl = $productUrl;

        return $this;
    }

    public function getGtin(): ?string
    {
        return $this->gtin;
    }

    public function getGtinList(): array
    {
        if ($gtins = self::cleanGtin($this->gtin)) {
            return explode(';', $gtins);
        }

        return [];
    }

    public function setGtin(?string $gtin): self
    {
        $this->gtin = self::cleanGtin($gtin);

        return $this;
    }

    public function getMpn(): ?string
    {
        return $this->mpn;
    }

    public function setMpn(?string $mpn): self
    {
        $this->mpn = $mpn;

        return $this;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function setSku(?string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getLocalId(): string
    {
        return $this->localId;
    }

    public function setLocalId(string $localId): self
    {
        $this->localId = $localId;

        return $this;
    }

    public function getAccount(): ?Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGroupId(): ?string
    {
        return $this->groupId;
    }

    public function setGroupId(?string $groupId): self
    {
        $this->groupId = $groupId;

        return $this;
    }

    public function getTraitAverages(): Collection
    {
        $productCategoryPath = $this->getCategory()->getPath();
        $paths = $this->getCategory()->getParentPaths();
        array_unshift($paths, $productCategoryPath);

        foreach ($paths as $path) {
            $filteredAverages = $this->traitAverages->filter(
                function (ProductTraitAverageGrade $element) use ($productCategoryPath) {
                    $elementPath = $element->getCategoryTrait()->getProductCategory()->getPath();

                    return 0 === strpos($productCategoryPath, $elementPath);
                }
            );

            if ($filteredAverages->count() > 0) {
                return new ArrayCollection($filteredAverages->getValues());
            }
        }

        return new ArrayCollection();
    }

    public function getTraitAverageByTrait(CategoryTrait $trait): ProductTraitAverageGrade
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq('categoryTrait', $trait));
        $average = $this->traitAverages->matching($criteria)->first();

        if (!$average) {
            $average = new ProductTraitAverageGrade();
            $average
                ->setAccount($this->getAccount())
                ->setCategoryTrait($trait)
                ->setProduct($this)
            ;
        }

        return $average;
    }

    public function getSourceType(): ?string
    {
        return $this->sourceType;
    }

    public function setSourceType(?string $sourceType): self
    {
        $this->sourceType = $sourceType;
        $this->updateTimestamps();

        return $this;
    }

    public static function cleanGtin(?string $raw): ?string
    {
        if (null === $raw) {
            return null;
        }

        $gtins = [];
        $rawGtins = explode(';', $raw);
        $gtinsCount = 0;

        foreach ($rawGtins as $rawGtin) {
            $gtin = trim($rawGtin);
            $length = mb_strlen($gtin);

            if ($length >= self::GTIN_MIN_LENGTH && $length <= self::GTIN_MAX_LENGTH && ctype_digit($gtin)) {
                $gtins[] = $gtin;
            }

            if (++$gtinsCount >= self::GTIN_MAX_COUNT) {
                break;
            }
        }

        return empty($gtins) ? null : join(';', $gtins);
    }

    public function getLastReviewAt(): ?DateTimeImmutable
    {
        return $this->lastReviewAt;
    }

    public function setLastReviewAt(?DateTimeInterface $lastReviewAt): self
    {
        $this->lastReviewAt = $lastReviewAt instanceof DateTime
            ? DateTimeImmutable::createFromMutable($lastReviewAt)
            : $lastReviewAt;

        return $this;
    }

    public function isProductUrlValid(): bool
    {
        if (!$this->productUrl) {
            return false;
        }

        $parsed = parse_url($this->productUrl);

        if (!isset($parsed['scheme'], $parsed['host'])) {
            return false;
        }

        if (!in_array($parsed['scheme'], ['http', 'https'])) {
            return false;
        }

        $host = "{$parsed['scheme']}://{$parsed['host']}";
        $path = mb_substr($this->productUrl, mb_strlen($host));
        $url = $host.implode("/", array_map("rawurlencode", explode("/", $path)));

        return (bool) filter_var($url, FILTER_VALIDATE_URL);
    }

    public function isDeactivated(): bool
    {
        return (bool) $this->deactivated;
    }

    public function setDeactivated(?bool $deactivated): self
    {
        $this->deactivated = $deactivated;

        return $this;
    }
}
